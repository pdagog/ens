    // void swtch (struct proc *elu)
    swtch:
	// sauvegarder les registres actuels dans l'ancienne pile, sauf :
	// - PC : déjà sauvegardé, c'est l'adresse de retour de swtch
	// - A : pas besoin car c'est la valeur de retour (quand il y en a une)
250	push %b
251	push %sr
	// sauvegarder sp dans l'ancien descripteur de processus
252	ld   [1000],%a		// a $\leftarrow$ curproc
253	st   %sp,[%a+5]		// curproc->kstack $\leftarrow$ sp

	// changer curproc : curproc $\leftarrow$ argument 1 = \texttt{elu}
254	ld   [%sp+3],%a		// a $\leftarrow$ elu
255	st   %a,[1000]		// curproc $\leftarrow$ a = elu

	// changer de pile noyau
256	ld   [%a+5],%sp		// sp $\leftarrow$ elu->kstack $\Rightarrow$ pile noyau du nouveau processus

	// restaurer les registres du nouveau processus
257	pop  %sr
258	pop  %b

259	rtn			// retour à l'appelant de swtch pour le nouveau processus
