calcul:
    // on ne peut pas faire
    // les calculs sans
    // backup dans la pile
    ld   [%sp+1],%a
    mul  [%sp+2],%a
    push %a
    ld   [%sp+4],%a
    mul  [%sp+5],%a
    add  [%sp],%a
    add  1,%sp
    rtn

somme_sauf:
    ld   [%sp+1],%a
    cmp  [%sp+3],%a
    jeq  a1
    add  [%sp+2],%a
    jmp  a2
a1: ld   [%sp+2],%a
// fin du if
a2: mul  2,%a
    rtn

somme_pairs_sauf:
    push %b
    // debut=sp+2, fin=sp+3, sauf=sp+4
    ld   [%sp+2],%b // B $\leftarrow$ i
    ld   0,%a	    // A $\leftarrow$ s
    jmp  b2
// début de la boucle
b1: cmp  [%sp+4],%b // comparer sauf à i
    jeq  b3         // saut si égaux
    add  %b,%a      // s += i
b3: add  2,%b       // i += 2
b2: cmp  [%sp+3],%b // comparer fin à i
    jge  b1         // saut si fin $\geq$ i
    pop  %b
    rtn
