// variable globale
int *p ;

void thread1 (...) {
    // x est dans la pile de thread1
    int x = 5 ;
    p = &x ;
    sleep (5) ;
    printf ("%d\n", x) ;
}

void thread2 (...) {
    sleep (2) ;
    *p = 2 ;
}
