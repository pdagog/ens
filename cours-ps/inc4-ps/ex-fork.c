pid_t v ;

switch (v = fork ())
{
    case -1 :		// erreur : ne pas oublier ce cas
	raler ("fork") ;

    case 0 :		// le fils :
	fils () ;	  // isoler le fils
	exit (0) ;	  // cordon sanitaire

    default :		// le père continue
	...
}
