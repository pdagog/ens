#define	CHK(op)	  do { if ((op)==-1) raler(#op); } while (0)

void raler (const char *msg)
{
    perror (msg) ;
    exit (1) ;
}

...

CHK (access ("titi", R_OK)) ;
...
CHK (fd = open ("toto", O_RDONLY)) ;
...
