struct sockaddr_un {
  uint16_t sun_family;            // \texttt{AF\_UNIX}
  char     sun_path[108];         // chemin
};
