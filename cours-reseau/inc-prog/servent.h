struct  servent {
  char *s_name;      // nom officiel
  char **s_aliases;  // liste de synonmyes
  int  s_port;       // numéro de port
  char *s_proto;     // protocole
}
