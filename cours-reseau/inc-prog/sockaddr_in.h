struct in_addr {
  uint32_t s_addr;
};

struct sockaddr_in {
  uint16_t       sin_family;      // \texttt{AF\_INET}
  uint16_t       sin_port;        // Port
  struct in_addr sin_addr;        // Adresse IP
  char           sin_zero[8];     // Bourrage
};
