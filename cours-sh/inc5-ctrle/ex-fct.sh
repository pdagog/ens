fact ()				# définition de la fonction
{
    local f			# variable locale
    local n=$1			# une fonction a des arguments

    if [ $n -le 1 ]
    then echo 1
    else
	f=$(fact $((n-1)) )	# appel récursif
	echo $((n*f))		# sortie standard
    fi
    return 0			# code de retour
}

fact 5 > resultat		# appel de la fonction + redirection
